<?php 
	
	class user_model extends CI_Model
	{
		public function get_users($userID, $userName) {
			/*$config['hostname'] = "localhost";
			$config['username'] = "root";
			$config['password'] = "";
			$config['database'] =  "user";

			$connection = $this->load->database($config);*/
			$this->db->where([
				'id' => $userID,
				'name' => $userName
			]);     
			$user_query = $this->db->get('user');
			// $query = $this->db->query("SELECT * FROM user");
			return $user_query->result_array();
		}
		public function insert_user($data) {
			$query = $this->db->insert('user', $data);

			/*$sql = "INSERT INTO user(name,password,email) 
					VALUES('name1','namepassword','nameemail@gmail.com')";

			$query = $this->db->query($sql);*/
			if($query) {
				return true;
			}else{
				return false;
			}
		}
		public function validTokenUpdate($email) {
			$data = array(
		        'email_token' => "verified"
			);
			$this->db->where('email', $email);
			$result = $this->db->update('user', $data);
			if($result) {
				return true;
			}else{
				return false;
			}
		}
		public function delete_user($id) {
			$this->db->where('id', $id);
			$result = $this->db->delete('user');
			if($result) {
				return "data deleted";
			}
		}
		public function login_auth($name, $password) {
			$this->db->where([
				'name' => $name,
				'password' => $password
			]);
			$this->db->limit(1);
			$query = $this->db->get('user');

			if($query->num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}
		}
		public function check_user($email, $email_token) {
			$this->db->where([
				'email' => $email
			]);   
			$this->db->limit(1);  
			$user_query = $this->db->get('user');
			foreach ($user_query->result_array() as $row) {
			    if( $row['email_token'] == $email_token ){
					return "true";
				}else if( $row['email_token'] == "verified" ) {
					return "false";
				}
		    }
		}

		public function test() {
			return true;
		}
	}

?>