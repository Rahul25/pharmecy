<?php 
	$this->load->view('layouts/head.php');
	$this->load->view('layouts/navbar.php');
?>

<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<h2>Login Form</h2>
			<?php
				if( $this->session->flashdata('err_valid_msg') ) {
					echo $this->session->flashdata('err_valid_msg');
				}

				$given_name = $this->session->userdata('name');
				$given_pass = $this->session->userdata('pass');
			?>
			<?php
				if( $this->session->flashdata('login_err_msg') ) {
					echo $this->session->flashdata('login_err_msg');
				}
				if( $this->session->flashdata('verify_success') ) {
					echo $this->session->flashdata('verify_success');
				}
				if( $this->session->flashdata('token_exp') ) {
					echo $this->session->flashdata('token_exp');
				}
				if( $this->session->flashdata('already_verified') ) {
					echo $this->session->flashdata('already_verified');
				}
			?>
			<?php 
				$attributes = array('id' =>'login_form', 'class'=> 'form-horizontal' );
				echo form_open('login/login', $attributes);
			?>

			<div class="form-group">
				<label for="username">User Name:</label>
				<input type="text" name="username" class="form-control" id="username" value="<?php echo $given_name; ?>">
			</div>
			<div class="form-group">
				<label for="password">Password:</label>
				<input type="password" name="password" class="form-control" id="password" value="<?php echo $given_pass; ?>">
			</div>
			<button type="submit" name="submit" class="btn btn-primary">Submit</button>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>

</body>
</html>

