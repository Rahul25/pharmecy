<body>

	<nav class="navbar navbar-toggleable-md navbar-light bg-custom">
	  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <a class="navbar-brand" href="<?php echo base_url(); ?>">Codeignitor</a>

	  <div class="collapse navbar-collapse" id="navbarSupportedContent">
	    <ul class="navbar-nav mr-auto">
	      <li class="nav-item active">
	        <a class="nav-link" href="<?php echo base_url(); ?>">Home <span class="sr-only">(current)</span></a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="<?php echo base_url('login'); ?>">Login</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="<?php echo base_url('register'); ?>">Register</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="<?php echo base_url('product'); ?>">Product</a>
	      </li>
	    </ul>
	  </div>
	</nav>