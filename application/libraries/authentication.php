<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load composer's autoloader
require 'vendor/autoload.php';

class authentication {
	public function send_mail($email, $email_token) {
		$mail = new PHPMailer(true);          // Passing `true` enables exceptions
		try {
		    $mail->IsSMTP();
		    $mail->SMTPDebug  = 0;
		    $mail->SMTPAuth   = true;
		    $mail->SMTPSecure = "tls";
		    $mail->Host       = "smtp.gmail.com";
		    $mail->Port       = 587;

		    $mail->AddAddress($email);
		    $mail->Username="rahulroy121025@gmail.com";
		    $mail->Password="gmailpassword";
		    $mail->SetFrom('rahulroy121025@gmail.com','Rahul Roy');
		    $mail->Subject    = "Your Account Activation Link";
		    $message =  "Please click this link to verify your account:
		       http://localhost/CodeIgniter/emailverification?email=".$email."&email_token=".$email_token;
		    $mail->MsgHTML($message);
		    $mail->Send();
		    return true;
		} catch (Exception $e) {
		    return $mail->ErrorInfo;
		}
	} 
}