<?php 

	class Emailverification extends CI_Controller {
		public function __construct() {
			parent::__construct();
		}
		public function index() {
			$email = $_GET['email'];
			$email_token = $_GET['email_token'];
			$this->load->model('user_model');
			$result = $this->user_model->check_user($email, $email_token);
			if( $result == "true" ) {
				$update = $this->user_model->validTokenUpdate($email);
				if($update) {
					$data['verify_success'] =  "you are now verified..please login";
					$this->session->set_flashdata($data);
					redirect('login');
				}else {
					$data['token_exp'] = "sorry!..token expired...register again";
					$this->session->set_flashdata($data);
				}
			}elseif ($result == "false") {
				$data['already_verified'] = "already verified...please login";
				$this->session->set_flashdata($data);
				redirect('login');
			}
		}
	}

?>