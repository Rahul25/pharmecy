<?php 

	class Login extends CI_Controller {

		public function __construct() {
	        parent::__construct();
	        $this->load->model('user_model');
	    }

	    public function index() {
			$this->load->view('user/login_form');
		}

		public function login() {
			
			$this->form_validation->set_rules('username', 'Username', 'trim|required',
						array('required' => 'Please enter a %s.')
			);
			$this->form_validation->set_rules('password', 'Password', 'trim|required',
                        array('required' => 'You must provide a %s.')
            );
            if ($this->form_validation->run() !== false){
                $q_res = $this->user_model->login_auth(
                							$this->input->post('username'),
                							$this->input->post('password')
                						);
                if($q_res){
                	$_SESSION['username'] = $this->input->post('username');
                	redirect("/");    // redirecting to home
                }else {
                	$data = array(
					    'login_err_msg' => 'Incorrect username or password'
					);
					$this->session->set_flashdata($data); 
                	//$this->load->view('user/login_form', $data);
                	redirect("login");
                }
            }else {
            	$data = array( 
            		'err_valid_msg' => validation_errors(),
            		'name' => $this->input->post('username'),
            		'pass' => $this->input->post('password')
            	);
            	$this->session->set_flashdata($data);
            	$this->session->set_userdata($data);
            	redirect("login");
            }
		}
	}

?>